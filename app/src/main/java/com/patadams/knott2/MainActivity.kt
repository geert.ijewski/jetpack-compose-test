package com.patadams.knott2

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.Composable
import androidx.compose.unaryPlus
import androidx.ui.core.Text
import androidx.ui.core.dp
import androidx.ui.core.setContent
import androidx.ui.foundation.DrawImage
import androidx.ui.graphics.Color
import androidx.ui.layout.*
import androidx.ui.material.*
import androidx.ui.material.surface.Surface
import androidx.ui.res.imageResource
import androidx.ui.tooling.preview.Preview

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MaterialTheme {
                BuildLayout()
            }
        }
    }
}

@Composable
fun BuildLayout() {
    // Use whole screen
    Column (modifier = ExpandedHeight,crossAxisAlignment = CrossAxisAlignment.Center){
        AppBar()

        // make this as big as possible
        Surface(color = Color(0x1b365d)) {
           Text("Content ASDASD")
        }


        Divider(color = Color.Black)

        NavBar()
    }
}

@Composable
fun AppBar() {
    TopAppBar(title = { Text("TopBar") },
        color = Color(0xFFFF5722),
        actionData = listOf("Test", "bla"))
    {
            string -> { println(string) }
    }
}

@Composable
fun NavBar() {
    BottomAppBar(color = Color(0xFFFF5722),
        actionData = listOf("Test", "bla"))
    {
        string -> { println(string) }
    }
}

@Preview
@Composable
fun DefaultPreview() {
    MaterialTheme {
        BuildLayout()
    }
}
